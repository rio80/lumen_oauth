<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class HomeController extends Controller
{

    public function index(){
        return "ini index";
    }

    public function errorLogin(Request $request){
        $code = 401;
        $response = [
            'status' => false,
            'statusCode' => $code,
            'url' => $request->path
        ];


        $response['message'] = "Unauthorized, you must login first";
        Log::critical($response['message'] . ' : '. $request->path);
        return response()->json($response, $code, []);
    }

}
