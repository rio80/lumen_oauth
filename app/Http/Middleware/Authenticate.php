<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as MiddlewareAuthenticate;
use Illuminate\Contracts\Auth\Factory as Auth;

class Authenticate
{
    /**
     * The authentication guard factory instance.
     *
     * @var \Illuminate\Contracts\Auth\Factory
     */
    protected $auth;

    /**
     * Create a new middleware instance.
     *
     * @param  \Illuminate\Contracts\Auth\Factory  $auth
     * @return void
     */
    public function __construct(Auth $auth)
    {
        $this->auth = $auth;
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {

        $full_path = $request->url();

        $parts = parse_url($full_path);
        $path_parts = explode('/', $parts['path']);
        $new_path = implode('/', $path_parts);
        $new_url = rtrim(env('APP_URL'), '/').$new_path;

         if ($this->auth->guard($guard)->guest()) {
            $route_login_error = env('APP_URL').'/error/login?path=' .$new_url;
            return redirect($route_login_error);
        }

        return $next($request);

    }


}
