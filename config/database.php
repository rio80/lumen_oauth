<?php

use Illuminate\Support\Str;

return [

    /*
    |--------------------------------------------------------------------------
    | Default Database Connection Name
    |--------------------------------------------------------------------------
    |
    | Here you may specify which of the database connections below you wish
    | to use as your default connection for all database work. Of course
    | you may use many connections at once using the Database library.
    |
    */

    'default' => env('DB_CONNECTION', 'mysql'),

    /*
    |--------------------------------------------------------------------------
    | Database Connections
    |--------------------------------------------------------------------------
    |
    | Here are each of the database connections setup for your application.
    | Of course, examples of configuring each database platform that is
    | supported by Laravel is shown below to make development simple.
    |
    |
    | All database work in Laravel is done through the PHP PDO facilities
    | so make sure you have the driver for your particular database of
    | choice installed on your machine before you begin development.
    |
    */

    'connections' => [

        'sqlite' => [
            'driver' => 'sqlite',
            'url' => env('DATABASE_URL'),
            'database' => env('DB_DATABASE', database_path('database.sqlite')),
            'prefix' => '',
            'foreign_key_constraints' => env('DB_FOREIGN_KEYS', true),
        ],

        'mysql' => [
            'driver' => 'mysql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '3306'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'unix_socket' => env('DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'pgsql' => [
            'driver' => 'pgsql',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', '127.0.0.1'),
            'port' => env('DB_PORT', '5432'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
            'schema' => 'public',
            'sslmode' => 'prefer',
        ],

        'sqlsrv' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('DB_HOST', 'localhost'),
            'port' => env('DB_PORT'),
            'database' => env('DB_DATABASE', 'forge'),
            'username' => env('DB_USERNAME', 'forge'),
            'password' => env('DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],

        'passion_db' => [
            'driver' => 'sqlsrv',
            'url' => env('DATABASE_URL'),
            'host' => env('passion_db_DB_HOST', 'localhost'),
            'port' => env('passion_db_DB_PORT'),
            'database' => env('passion_db_DB_DATABASE', 'forge'),
            'username' => env('passion_db_DB_USERNAME', 'forge'),
            'password' => env('passion_db_DB_PASSWORD', ''),
            'charset' => 'utf8',
            'prefix' => '',
            'prefix_indexes' => true,
        ],
		
		'mytrv_db' => [
            'driver'        => 'oracle',
            'tns'           => env('MYTRV_DB_TNS', ''),
            'host'          => env('MYTRV_DB_HOST', ''),
            'port'          => env('MYTRV_DB_PORT', ''),
            'database'      => env('MYTRV_DB_DATABASE', ''),
            'username'      => env('MYTRV_DB_USERNAME', ''),
            'password'      => env('MYTRV_DB_PASSWORD', ''),
            'charset'       => env('MYTRV_DB_CHARSET', 'AL32UTF8'),
            'prefix'        => env('MYTRV_DB_PREFIX', ''),
            'prefix_schema' => env('MYTRV_DB_SCHEMA_PREFIX', ''),
            // 'edition'       => env('ORCL_DB_EDITION', 'ora$base'),
        ],
		 'tcups1158_db' => [
            'driver'        => 'oracle',
            'tns'           => env('ORCLtcups1158_DB_TNS', ''),
            'host'          => env('ORCLtcups1158_DB_HOST', ''),
            'port'          => env('ORCLtcups1158_DB_PORT', '1521'),
            'database'      => env('ORCLtcups1158_DB_DATABASE', ''),
            'username'      => env('ORCLtcups1158_DB_USERNAME', ''),
            'password'      => env('ORCLtcups1158_DB_PASSWORD', ''),
            'charset'       => env('ORCLtcups1158_DB_CHARSET', 'AL32UTF8'),
            'prefix'        => env('ORCLtcups1158_DB_PREFIX', ''),
            'prefix_schema' => env('ORCLtcups1158_DB_SCHEMA_PREFIX', ''),
            // 'edition'       => env('ORCLtcups1158_DB_EDITION', 'ora$base'),
        ],

        'PrepaidDB' => [
            'driver'         => 'oracle',
            'tns'            => env('PrepaidDB_TNS', ''),
            'host'           => env('PrepaidDB_HOST', ''),
            'port'           => env('PrepaidDB_PORT', '1521'),
            'database'       => env('PrepaidDB_DB_DATABASE', ''),
            'username'       => env('PrepaidDB_DB_USERNAME', ''),
            'password'       => env('PrepaidDB_DB_PASSWORD', ''),
            'charset'        => env('PrepaidDB_DB_CHARSET', 'AL32UTF8'),
            'prefix'         => env('PrepaidDB_DB_PREFIX', ''),
            'prefix_schema'  => env('PrepaidDB_DB_SCHEMA_PREFIX', ''),
            'edition'        => env('PrepaidDB_DB_EDITION', 'ora$base'),

        ],
        'AMDWH' => [
            'driver'         => 'oracle',
            'tns'            => env('AMDWH_TNS', ''),
            'host'           => env('AMDWH_HOST', ''),
            'port'           => env('AMDWH_PORT', '1521'),
            'database'       => env('AMDWH_DB_DATABASE', ''),
            'username'       => env('AMDWH_DB_USERNAME', ''),
            'password'       => env('AMDWH_DB_PASSWORD', ''),
            'charset'        => env('AMDWH_DB_CHARSET', 'AL32UTF8'),
            'prefix'         => env('AMDWH_DB_PREFIX', ''),
            'prefix_schema'  => env('AMDWH_DB_SCHEMA_PREFIX', ''),
            'edition'        => env('AMDWH_DB_EDITION', 'ora$base'),

        ],
        'PegasusDB' => [
            'driver'         => 'oracle',
            'tns'            => env('PegasusDB_TNS', ''),
            'host'           => env('PegasusDB_HOST', ''),
            'port'           => env('PegasusDB_PORT', '1521'),
            'database'       => env('PegasusDB_DB_DATABASE', ''),
            'username'       => env('PegasusDB_DB_USERNAME', ''),
            'password'       => env('PegasusDB_DB_PASSWORD', ''),
            'charset'        => env('PegasusDB_DB_CHARSET', 'AL32UTF8'),
            'prefix'         => env('PegasusDB_DB_PREFIX', ''),
            'prefix_schema'  => env('PegasusDB_DB_SCHEMA_PREFIX', ''),
            'edition'        => env('PegasusDB_DB_EDITION', 'ora$base'),

        ],

        'CMS' => [
            'driver'         => 'oracle',
            'tns'            => env('CMS_DB_TNS', ''),
            'host'           => env('CMS_DB_HOST', ''),
            'port'           => env('CMS_DB_PORT', '1521'),
            'database'       => env('CMS_DB_DATABASE', ''),
            'username'       => env('CMS_DB_USERNAME', ''),
            'password'       => env('CMS_DB_PASSWORD', ''),
            'charset'        => env('CMS_DB_CHARSET', 'AL32UTF8'),
            'prefix'         => env('CMS_DB_PREFIX', ''),
            'prefix_schema'  => env('CMS_DB_SCHEMA_PREFIX', ''),
            'edition'        => env('CMS_DB_EDITION', 'ora$base'),

        ],

        'h2h' => [
            'driver' => 'mysql',
            'url' => env('H2H_DATABASE_URL'),
            'host' => env('H2H_DB_HOST', '127.0.0.1'),
            'port' => env('H2H_DB_PORT', '3306'),
            'database' => env('H2H_DB_DATABASE', 'forge'),
            'username' => env('H2H_DB_USERNAME', 'forge'),
            'password' => env('H2H_DB_PASSWORD', ''),
            'unix_socket' => env('H2H_DB_SOCKET', ''),
            'charset' => 'utf8mb4',
            'collation' => 'utf8mb4_unicode_ci',
            'prefix' => '',
            'prefix_indexes' => true,
            'strict' => true,
            'engine' => null,
            'options' => extension_loaded('pdo_mysql') ? array_filter([
                PDO::MYSQL_ATTR_SSL_CA => env('MYSQL_ATTR_SSL_CA'),
            ]) : [],
        ],

        'dealership_db' => [
            'driver'        => 'oracle',
            'tns'           => env('DEALERSHIP_DB_TNS', ''),
            'host'          => env('DEALERSHIP_DB_HOST', ''),
            'port'          => env('DEALERSHIP_DB_PORT', ''),
            'database'      => env('DEALERSHIP_DB_DATABASE', ''),
            'username'      => env('DEALERSHIP_DB_USERNAME', ''),
            'password'      => env('DEALERSHIP_DB_PASSWORD', ''),
            'charset'       => env('DEALERSHIP_DB_CHARSET', 'AL32UTF8'),
            'prefix'        => env('DEALERSHIP_DB_PREFIX', ''),
            'prefix_schema' => env('DEALERSHIP_DB_SCHEMA_PREFIX', ''),
            // 'edition'       => env('ORCL_DB_EDITION', 'ora$base'),
        ],
        'paybil_db' => [
            'driver'        => 'oracle',
            'tns'           => env('PAYBIL_DB_TNS', ''),
            'host'          => env('PAYBIL_DB_HOST', ''),
            'port'          => env('PAYBIL_DB_PORT', ''),
            'database'      => env('PAYBIL_DB_DATABASE', ''),
            'username'      => env('PAYBIL_DB_USERNAME', ''),
            'password'      => env('PAYBIL_DB_PASSWORD', ''),
            'charset'       => env('PAYBIL_DB_CHARSET', 'AL32UTF8'),
            'prefix'        => env('PAYBIL_DB_PREFIX', ''),
            'prefix_schema' => env('PAYBIL_DB_SCHEMA_PREFIX', ''),
            // 'edition'       => env('ORCL_DB_EDITION', 'ora$base'),
        ],

    ],

    /*
    |--------------------------------------------------------------------------
    | Migration Repository Table
    |--------------------------------------------------------------------------
    |
    | This table keeps track of all the migrations that have already run for
    | your application. Using this information, we can determine which of
    | the migrations on disk haven't actually been run in the database.
    |
    */

    'migrations' => 'migrations',

    /*
    |--------------------------------------------------------------------------
    | Redis Databases
    |--------------------------------------------------------------------------
    |
    | Redis is an open source, fast, and advanced key-value store that also
    | provides a richer body of commands than a typical key-value system
    | such as APC or Memcached. Laravel makes it easy to dig right in.
    |
    */

    'redis' => [

        'client' => env('REDIS_CLIENT', 'phpredis'),

        'options' => [
            'cluster' => env('REDIS_CLUSTER', 'redis'),
            'prefix' => env('REDIS_PREFIX', Str::slug(env('APP_NAME', 'laravel'), '_').'_database_'),
        ],

        'default' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_DB', '0'),
        ],

        'cache' => [
            'url' => env('REDIS_URL'),
            'host' => env('REDIS_HOST', '127.0.0.1'),
            'password' => env('REDIS_PASSWORD', null),
            'port' => env('REDIS_PORT', '6379'),
            'database' => env('REDIS_CACHE_DB', '1'),
        ],

    ],

];