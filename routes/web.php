<?php
use Illuminate\Http\Request;
/** @var \Laravel\Lumen\Routing\Router $router */

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

 $router->group(['prefix' => 'test', 'middleware' => 'auth:api'], function($router){
    $router->get('/endpoint',  function (Request $request) {
        echo "tesss oauth";
    });
});

$router->get('/home', [
    'as' => 'home', 'uses' => 'HomeController@index'
]);

$router->get('error/login', [
    'as' => 'error.login', 'uses' => 'HomeController@errorLogin'
]);

